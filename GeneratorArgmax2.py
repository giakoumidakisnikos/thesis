import math

input_size = 32
weights = [[[11, 7, 14, -10], [7, 15, -11, 2]], [[11, 7], [12, 8]]]
weight_width = 4
width = 8
previous_wire_width = 0
exits = len(weights[-1])

wirename = "prod"
inname = "in"
argmax_bits = 0

# total width 
out_width = ((input_size//8) + width*weight_width)

def neuron(weights, z):
     # for loop για κάθε neuron
     
    for y in range(len(weights[z])):
        # άδειο string για να κρατάω τα prod
        global previous_wire_width
        global inname
        global argmax_bits
        out_0_w = ""
        # for loop για κάθε prod
        for x in range(len(weights[z][y])):
            # υπολογισμός wire lenght 
            lsb = width*x
            msb = lsb+width-1
            if (previous_wire_width != 0):
                inname = "out_n0_w" + str(x) + "_" + str(z-1)
                lsb = 0
                msb = width
            # κρατάω το prod σε κάθε loop
            out_0_w = out_0_w + wirename + "_" + str(x) + "_" + "w" + str(y) + "_" + str(z) + " + " 
            
            print("wire signed[%d:0] %s_%d_w%d_%d;" % (width+weight_width, wirename, x, y, z ))
            
            print("assign %s_%d_w%d_%d = (%s[%d:%d] * %d);" % (wirename, x, y, z, inname, msb, lsb, weights[z][y][x]))
            
        # print sum
        print("wire signed[%d:0] out_0_w%d_%d;" % (width+weight_width+len(weights[z][y]), y, z ))
        out_0_w = out_0_w + "bias"
        print("assign out_0_w%d_%d = %s;" % (y, z, out_0_w))
        
        #print relu
        print("wire [%d:0] out_n0_w%d_%d;" % (width+weight_width+len(weights[z][y]), y, z ))
        print("assign out_n0_w%d_%d = (out_0_w%d_%d[%d]==1)? 0 : $unsigned(out_0_w%d_%d[%d:0]);\n" % (y, z, y, z, width + weight_width+len(weights[z][y]), y, z, width+weight_width + len(weights[z][y])-1))
        argmax_bits = width+weight_width + len(weights[z][y])
    previous_wire_width = width + weight_width+len(weights[z][y])-1

def argmax(exits, bits):
    s = math.log2(exits)
    print("wire [%d:0] in;" % (exits*bits-1));
    print("wire [%d:0] max;" % (bits-1));
    print("wire [%d:0] ind;\n" % (s-1));
    
    print("wire [%d:0] max_i [%d:0];" % (bits-1, exits-1));
    print("wire [%d:0] ind_i [%d:0];" %  (s, exits-1));
    print("wire greater [%d:0];\n" % (exits-1));
    
    print("assign greater[0] = 0;")
    print("assign max_i[0] = in[%d:0];" % (bits-1))
    print("assign ind_i[0] = 0;\n")
    
    print("genvar g;")
    print("genvar N;")
    print("genvar M;\n")
    for i in range(exits):
        print("assign in[%d:%d]= out_n0_w%d_1;" % (bits*(i+1) -1, (bits*(i+1) - bits), i))
    print("generate\n");
    print("\tfor (g=1;g<N;g=g+1) begin:S1")
    print("\t\tassign greater[g] = (max_i[g-1] > in[(g+1)*M-1:g*M]);")
    print("\t\tassign max_i[g] = (greater[g]) ? max_i[g-1]: in[(g+1)*M-1:g*M];")
    print("\t\tassign ind_i[g] = (greater[g]) ? ind_i[g-1]: g;")
    print("\tend\n")
    print("endgenerate")
    
    print("assign max = max_i[%d];" % (exits-1))
    print("assign ind = ind_i[%d];" % (exits-1))

    
print ("module neuron (bias,in,out);\n")
print ("input signed [3:0] bias;")
print ("input [31:0] in;")
print ("output [22:0] out;\n")
# for loop για κάθε layer
for z in range(len(weights)):
    if (previous_wire_width != 0):
        width = previous_wire_width
    neuron(weights, z)
    print("\n\n\n")
if (exits > 1):
    argmax(exits, argmax_bits)
print ("endmodule")