module neuron (bias,in,out);

  input signed [3:0] bias;
  input [31:0] in;
  output [22:0] out;
 

//layer 0 neuron 0  
  
//w1=-7, 4'sb1001
 wire signed [12:0] prod1_w0; 
 assign prod1_w0= $signed({1'b0, in[7:0]}) * 4'sb1001;
 
//w2=6, 4'sb0110  
  wire signed [12:0] prod2_w0; 
  assign prod2_w0= $signed({1'b0, in[15:8]}) * 4'sb0110;
  
//w3=-5, 4'sb1011   
  wire signed [12:0] prod3_w0;
  assign prod3_w0= $signed({1'b0, in[23:16]}) * 4'sb1011;
  
//w4=3, 4'sb0011  
  wire signed [12:0] prod4_w0;
  assign prod4_w0= $signed({1'b0, in[31:23]}) * 4'sb0011;
 
 //sum
  wire signed [16:0] out_w0;
  assign out_w0 = bias+ prod1_w0+prod2_w0+prod3_w0+prod4_w0;

//relu
wire [16:0] outn0_w;
 assign outn0_w = ( out_w0[16]== 1)? 0 : $unsigned(out_w0[15:0]);

  
//layer 0 neuron 1

//w1=-6, 4'sb1010
 wire signed [12:0] prod1_w1; 
 assign prod1_w1 = $signed({1'b0, in[7:0]}) * 4'sb1010;
 
//w2=5, 4'sb0101  
  wire signed [12:0] prod2_w1; 
  assign prod2_w1 = $signed({1'b0, in[15:8]}) * 4'sb0101;
  
//w3=-1, 4'sb1111  
  wire signed [12:0] prod3_w1;
  assign prod3_w1 = $signed({1'b0, in[23:16]}) * 4'sb1111;
  
//w4=2, 4'sb0010 
  wire signed [12:0] prod4_w1;
  assign prod4_w1 = $signed({1'b0, in[31:23]}) * 4'sb0010;
 
 //sum
  wire signed [16:0] out_w1;
  assign out_w1 = bias+ prod1_w1+prod2_w1+prod3_w1+prod4_w1;

//relu
wire [16:0] outn1_w;
 assign outn1_w = ( out_w1[16]== 1)? 0 : $unsigned(out_w1[15:0]);
 
 
 //output neuron
 
 //w1=-2, 4'sb1110
  wire signed [20:0] prod1_wout; 
  assign prod1_wout = $signed({1'b0, outn0_w[15:0]}) * 4'sb1110;
 
 //w2=2, 4'sb0010  
  wire signed [20:0] prod2_wout; 
  assign prod2_wout = $signed({1'b0, outn1_w[15:0]}) * 4'sb0010;
  
 //sum
  wire signed [22:0] out_wout;
  assign out_wout = bias+ prod1_wout+prod2_wout;
  
 //relu
  wire [22:0] out_w;
  assign out_w = ( out_wout[22]== 1)? 0 : $unsigned(out_wout[21:0]);
  

assign out = out_w;
  
   
 
 
 
endmodule