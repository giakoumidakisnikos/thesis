module neuron (bias,in,out);

  input signed [3:0] bias;
  input [31:0] in;
  output [21:0] out;
 

//layer 0 neuron 0  
  
//w1=-7, 4'sb1001
 wire signed [12:0] prod1_w0; 
 assign prod1_w0= $signed({1'b0, in[7:0]}) * 4'sb1001;
 
//w2=6, 4'sb0110  
  wire signed [12:0] prod2_w0; 
  assign prod2_w0= $signed({1'b0, in[15:8]}) * 4'sb0110;
  
//w3=-5, 4'sb1011   
  wire signed [12:0] prod3_w0;
  assign prod3_w0= $signed({1'b0, in[23:16]}) * 4'sb1011;
  
//w4=3, 4'sb0011  
  wire signed [12:0] prod4_w0;
  assign prod4_w0= $signed({1'b0, in[31:23]}) * 4'sb0011;
 
 //sum
  wire signed [16:0] out_w0;
  assign out_w0 = bias+ prod1_w0+prod2_w0+prod3_w0+prod4_w0;

//relu
wire [16:0] outn0_w;
 assign outn0_w = ( out_w0[16]== 1)? 0 : $unsigned(out_w0[15:0]);

 
 endmodule
